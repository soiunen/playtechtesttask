//event listener to catch button click to add new tests
document.getElementById('check').addEventListener('click',
    () => {
        if (document.getElementById('word').value !== '') {
            let l = document.getElementsByTagName('tr').length - 1;
            addTR(document.getElementById('word').value, l + 1);
        } else {
            alert('Please insert a word to check.');
        }

    });

//function to check palindrome, returns true or false
function isPalindrome(word) {
    word = word.toLowerCase().trim();
    document.getElementById('word').value = '';
    return Array.from(word).toString() === Array.from(word).reverse().toString();
}

//tests provided by task
console.log(isPalindrome("anna") === true);
console.log(isPalindrome("Anna") === true);
console.log(isPalindrome("anna ") === true);
console.log(isPalindrome("YellowSubmarine") === false);

//display provided tests in front-end
let arr = ["anna", "Anna", "anna ", "YellowSubmarine"];
arr.forEach((item, c = 1) => {
    addTR(item, c + 1);
});

//function to add table rows
function addTR(word, counter) {
    let tr = document.createElement('TR');
    let sequenceTH = document.createElement('th');
    sequenceTH.setAttribute("scope", "row");
    let inputTD = document.createElement('td');
    let expectedTD = document.createElement('td');
    let sequence = document.createTextNode(counter);
    let input = document.createTextNode(word);
    let expected = document.createTextNode(isPalindrome(word));
    expectedTD.classList.add('text-white');
    if (isPalindrome(word) === true) {
        expectedTD.classList.add('bg-success');
    } else {
        expectedTD.classList.add('bg-danger');
    }
    sequenceTH.appendChild(sequence);
    inputTD.appendChild(input);
    expectedTD.appendChild(expected);
    tr.appendChild(sequenceTH);
    tr.appendChild(inputTD);
    tr.appendChild(expectedTD);

    document.getElementById('tBody').appendChild(tr);
}
